# README #
This is an implementation of the "Optimization as a Model for Few-Shot Learning" paper in PyTorch and in TensorFlow.
There are two ipython notebooks which contain explanation of what each step does. 

### What is this repository for? ###

This is for the purpose of ICLR Reproducibility challenge as a part of the coursework for 10-605.

### Setup Instructions ###
Please use the link https://tiny-imagenet.herokuapp.com/ to get the Tiny ImageNet. 
Please use this downloaded path in cell 2 in the OptimizationModel-LearningToLearn_PyTorch.ipynb

